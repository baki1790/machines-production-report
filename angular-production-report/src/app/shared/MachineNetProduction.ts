export class MachineNetProduction {

    public machineName: String;
    public dateTimeFrom: Date;
    public dateTimeTo: Date;
    public netProductionAmount: Number;
    
}