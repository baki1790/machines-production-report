export class MachineNetPerHoursProduction {
    constructor(
        public machineName: String,
        public dateTimeFrom: Date,
        public dateTimeTo: Date,
        public hourlyNetProductionAmount: Map<Number, Number>
    ) {}

}