export class MachineScrapProduction {

    public machineName: String;
    public dateTimeFrom: Date;
    public dateTimeTo: Date;
    public scrapPercentageValue: Number;
    
}