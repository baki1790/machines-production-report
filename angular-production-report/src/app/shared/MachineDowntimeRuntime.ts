export class MachineDowntimeRuntime {

    public machineName: String;
    public dateTimeFrom: Date;
    public dateTimeTo: Date;
    public downtimePercentage: Number;
}