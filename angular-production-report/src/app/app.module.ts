import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatCardModule,      
  MatFormFieldModule,
  MatInputModule,      
  MatDatepickerModule, 
  MatNativeDateModule,
  MatSelectModule,      
  MatOptionModule,
  MatTableModule,
  MatButtonModule,
  MatDividerModule} from '@angular/material';

import { MachinesReportFormComponent } from './machines-report-form/machines-report-form.component';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MachinesNettoDataComponent } from './machines-netto-data/machines-netto-data.component';
import { MachinesScrapDataComponent } from './machines-scrap-data/machines-scrap-data.component';
import { MachinesRuntimeDataComponent } from './machines-runtime-data/machines-runtime-data.component';
import { MachinesNettoHourlyDataComponent } from './machines-netto-hourly-data/machines-netto-hourly-data.component';

@NgModule({
  declarations: [
    AppComponent,
    MachinesReportFormComponent,
    MachinesNettoDataComponent,
    MachinesScrapDataComponent,
    MachinesRuntimeDataComponent,
    MachinesNettoHourlyDataComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,      
    MatFormFieldModule,
    MatInputModule,      
    MatDatepickerModule, 
    MatNativeDateModule,
    MatSelectModule,      
    MatOptionModule,
    MatTableModule,
    MatDividerModule,
    FormsModule,  ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
