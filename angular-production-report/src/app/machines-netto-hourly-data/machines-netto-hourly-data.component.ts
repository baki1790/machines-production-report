import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MachineReportService } from '../service/machine-report.service';
import { MachineNetPerHoursProduction } from '../shared/MachineNetPerHoursProduction';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-machines-netto-hourly-data',
  templateUrl: './machines-netto-hourly-data.component.html',
  styleUrls: ['./machines-netto-hourly-data.component.css']
})
export class MachinesNettoHourlyDataComponent implements OnInit {

  chart = [];
  
  displayData: MachineNetPerHoursProduction[];
  constructor(private machinesReportService: MachineReportService) { }

  ngOnInit() {
    this.machinesReportService.machinesNettoHourlyReport
    .subscribe(machinesNettoHourlyData => {
      console.log(machinesNettoHourlyData)
      this.displayData = machinesNettoHourlyData
      console.log(machinesNettoHourlyData);
      
    })
  }

}
