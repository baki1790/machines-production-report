import { Component, OnInit } from '@angular/core';
import { MachineNetProduction } from '../shared/MachineNetProduction';
import { MatTableDataSource } from '@angular/material';
import { MachineReportService } from '../service/machine-report.service';

@Component({
  selector: 'app-machines-netto-data',
  templateUrl: './machines-netto-data.component.html',
  styleUrls: ['./machines-netto-data.component.css']
})
export class MachinesNettoDataComponent implements OnInit {

  displayedColumns: string[] = ['machineName', 'dateTimeFrom', 'dateTimeTo', 'netProductionAmount'];
  displayData = new MatTableDataSource<MachineNetProduction>();

  constructor(private machinesReportService: MachineReportService) { }

  ngOnInit() {
    this.machinesReportService.machinesNettoReport
    .subscribe(machinesNettoData => {
      this.displayData.data = machinesNettoData
    })
  }

}
