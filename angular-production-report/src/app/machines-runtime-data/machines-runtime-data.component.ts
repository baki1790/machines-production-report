import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MachineReportService } from '../service/machine-report.service';
import { MachineDowntimeRuntime } from '../shared/MachineDowntimeRuntime';

@Component({
  selector: 'app-machines-runtime-data',
  templateUrl: './machines-runtime-data.component.html',
  styleUrls: ['./machines-runtime-data.component.css']
})
export class MachinesRuntimeDataComponent implements OnInit {

  displayedColumns: string[] = ['machineName', 'dateTimeFrom', 'dateTimeTo', 'downtimePercentage'];
  displayData = new MatTableDataSource<MachineDowntimeRuntime>();

  constructor(private machinesReportService: MachineReportService) { }

  ngOnInit() {
    this.machinesReportService.machinesDowntimeReport
    .subscribe(machinesDowntimeData => {
      this.displayData.data = machinesDowntimeData
    })
  }

}
