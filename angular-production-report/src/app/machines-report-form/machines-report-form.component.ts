import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MachineReportService } from '../service/machine-report.service';

export interface ReportType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-machines-report-form',
  templateUrl: './machines-report-form.component.html',
  styleUrls: ['./machines-report-form.component.css']
})
export class MachinesReportFormComponent implements OnInit {

  private datePipe: DatePipe;
  machinesReportForm: FormGroup;
  date: Date;
  reportType:String;
  maxDate: Date;
  minDate: Date;
  reportTypes: ReportType[] = [
    {value: 'netto', viewValue: 'Netto production'},
    {value: 'scrap', viewValue: 'Scrap Percentage'},
    {value: 'downtime', viewValue: 'Downtime Percentage'},
    {value: 'netto_hourly', viewValue: 'Netto Production/Hour'}
  ];
  constructor(private formBuilder: FormBuilder, 
    private machinedReportService: MachineReportService) {
 
   }

  ngOnInit() {
    this.datePipe = new DatePipe('en-US');
    this.createMachinesReportForm();
  }

  private createMachinesReportForm(){
    this.maxDate = new Date("2018-01-07"); 
    this.minDate = new Date("2018-01-01");

    this.machinesReportForm = this.formBuilder.group({
      date: [Validators.required],
      reportType: ['',[Validators.required]]
    })
  }

  onSubmitMachinesReportForm() {
    console.log(this.machinesReportForm.value.date);
    console.log(this.machinesReportForm.value.reportType);
    const stringDate = this.datePipe.transform(this.machinesReportForm.value.date, 'dd-MM-yyyy');

    this.machinedReportService.getNettoMachinesReport(stringDate, this.machinesReportForm.value.reportType);
  }

  printDate(date: Date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy');
  }

}
