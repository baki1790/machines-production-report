import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MachineNetProduction } from '../shared/MachineNetProduction';
import { Subject, Observable } from 'rxjs';
import { MachineScrapProduction } from '../shared/MachineScrapProduction';
import { MachineDowntimeRuntime } from '../shared/MachineDowntimeRuntime';
import { MachineNetPerHoursProduction } from '../shared/MachineNetPerHoursProduction';

@Injectable({
  providedIn: 'root'
})
export class MachineReportService {

  private restApiUrl = "http://localhost:8080/";
  private NETTO: String = "netto";
  private SCRAP: String = "scrap";
  private DOWNTIME: String = "downtime";
  private NETTO_HOURLY: String = "netto_hourly";

  private machineNettoReportSource;
  private machineScrapReportSource;
  private machineDowntimeReportSource;
  private machineNettoHourlyReportSource;

  constructor(private httpClient: HttpClient) {
    this.machineNettoReportSource = new Subject<MachineNetProduction[]>();
    this.machineScrapReportSource = new Subject<MachineScrapProduction[]>();
    this.machineDowntimeReportSource = new Subject<MachineDowntimeRuntime[]>();
    this.machineNettoHourlyReportSource = new Subject<MachineNetPerHoursProduction[]>();
  }

  getNettoMachinesReport(date: String, reportType: String) {
    switch(reportType) {
      case this.NETTO: {
        this.httpClient.get<MachineNetProduction[]>(this.restApiUrl + '/' + this.NETTO + '/' + date)
        .subscribe(fetchedMachinesNettoReports => { this.machineNettoReportSource.next(fetchedMachinesNettoReports) });
        break;
      }
      case this.SCRAP: {
        this.httpClient.get<MachineScrapProduction[]>(this.restApiUrl + '/' + this.SCRAP + '/' + date)
        .subscribe(fetchedMachinesScrapReports => { this.machineScrapReportSource.next(fetchedMachinesScrapReports) });
        break;
      }
      case this.DOWNTIME: {
        this.httpClient.get<MachineDowntimeRuntime[]>(this.restApiUrl + '/' + this.DOWNTIME + '/' + date)
        .subscribe(fetchedMachinesDowntimeReports => { this.machineDowntimeReportSource.next(fetchedMachinesDowntimeReports) });
        break;
      }
      case this.NETTO_HOURLY: {
        this.httpClient.get<MachineNetPerHoursProduction[]>(this.restApiUrl + '/' + this.NETTO_HOURLY + '/' + date)
        .subscribe(fetchedMachinesNettoHourlyReports => { this.machineNettoHourlyReportSource.next(fetchedMachinesNettoHourlyReports) });
        break;
      }
      default: { 
        console.log("No Rest API available !"); 
        break; 
     } 
    }
     
  }

  get machinesNettoReport(): Observable<MachineNetProduction[]> {
    return this.machineNettoReportSource.asObservable();
  }

  get machinesScrapReport(): Observable<MachineScrapProduction[]> {
    return this.machineScrapReportSource.asObservable();
  }

  get machinesDowntimeReport(): Observable<MachineDowntimeRuntime[]> {
    return this.machineDowntimeReportSource.asObservable();
  }

  get machinesNettoHourlyReport(): Observable<MachineNetPerHoursProduction[]> {
    return this.machineNettoHourlyReportSource.asObservable();
  }

}
