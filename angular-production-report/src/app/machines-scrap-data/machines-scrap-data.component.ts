import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { MachineScrapProduction } from '../shared/MachineScrapProduction';
import { MachineReportService } from '../service/machine-report.service';

@Component({
  selector: 'app-machines-scrap-data',
  templateUrl: './machines-scrap-data.component.html',
  styleUrls: ['./machines-scrap-data.component.css']
})
export class MachinesScrapDataComponent implements OnInit {

  displayedColumns: string[] = ['machineName', 'dateTimeFrom', 'dateTimeTo', 'scrapPercentage'];
  displayData = new MatTableDataSource<MachineScrapProduction>();
  
  constructor(private machinesReportService: MachineReportService) { }

  ngOnInit() {
    this.machinesReportService.machinesScrapReport
    .subscribe(machinesScrapData => {
      this.displayData.data = machinesScrapData
    })
  }

}
