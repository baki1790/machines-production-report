# machines-production-report

This is a repository for development assesment required for recruitment process in Marviq company

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Project is composed from front end and backend clients.
    Backend:
    Springboot: version 2.2
    Java: version 1.8
    Maven: version 3.6
    Intellij Community Edition
----

    Frontend:
    Node.js LTS
    Angular Framework 8
    Visual Studio Code

## Pre Requirements
1) Installed Java = 1.8 
2) Installed Maven = 3.6
3) Installed Node.js
4) Angular CLI

## Runnning Application
    **Backend**: 
    1. from root springboot-production-report directory -> ***mvn clean install***
    2. from root springboot-production-report directory -> ***mvn spring-boot:run***
    
    **Frontend**:
    1. from root angular-production-report directory -> ***npm install***
    2. from root angular-production-report directory -> ***ng serve***
## Application on a browser is available on http://localhost:4200

----
#### TO DO
1. Map Hourly Netto Production to a chart. Display stasuses of temperature of machines on frontend site.  
2. Handling error on Backend side, for instance: unavailable database connection
3. Writing tests on frontend and backend site
4. Validation of rest service on backend site(created only on front end) 
7. Create documentation of services