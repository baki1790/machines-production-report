package com.lakomy.marviq.springbootproductionreport.aggregator.impl;

import com.lakomy.marviq.springbootproductionreport.aggregator.DataAggregator;
import com.lakomy.marviq.springbootproductionreport.model.MachineDataType;
import com.lakomy.marviq.springbootproductionreport.model.production.Production;
import com.lakomy.marviq.springbootproductionreport.model.runtime.Runtime;
import com.lakomy.marviq.springbootproductionreport.repository.ProductionRepository;
import com.lakomy.marviq.springbootproductionreport.repository.RuntimeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Component
@Slf4j
public class DataAggregatorImpl implements DataAggregator {

    private final ProductionRepository productionRepository;
    private final RuntimeRepository runtimeRepository;

    public DataAggregatorImpl(ProductionRepository productionRepository, RuntimeRepository runtimeRepository) {
        this.productionRepository = productionRepository;
        this.runtimeRepository = runtimeRepository;
    }

    @Override
    @Cacheable("productionData")
    public Map<String, List<Production>> getGroupedByMachinesProductionDataOnGivenDate(LocalDateTime startSearchDateTime) {
        LocalDateTime endSearchDateTime = startSearchDateTime.plusDays(1);
        log.info("Fetching production data from DB with startDate: " + startSearchDateTime + " end end date: " + endSearchDateTime);
        return groupProductionDataByMachinesName(productionRepository.getMachinesDataFromProductionByDateTime(startSearchDateTime, endSearchDateTime));
    }

    @Override
    @Cacheable("runtimeData")
    public Map<String, List<Runtime>> getGroupedByMachinesRuntimeDataOnGivenDate(LocalDateTime startSearchDateTime) {
        LocalDateTime endSearchDateTime = startSearchDateTime.plusDays(1);
        log.info("Fetching runtime data from DB with startDate: " + startSearchDateTime + " end end date: " + endSearchDateTime);
        return runtimeRepository.getMachinesDataFromProductionByDateTime(startSearchDateTime, endSearchDateTime)
                .stream().collect(groupingBy(Runtime::getMachineName));
    }

    @Override
    @Cacheable("temperatureData")
    public Map<String, List<Production>> getGroupedByMachinesTemperatureProductionDataOnGivenDateAndType(LocalDateTime startSearchDateTime, MachineDataType dataType) {
        LocalDateTime endSearchDateTime = startSearchDateTime.plusDays(1);
        log.info("Fetching runtime data from DB with startDate: " + startSearchDateTime + " end end date: " + endSearchDateTime);
        return groupProductionDataByMachinesName(productionRepository.getMachinesDataFromProductionByDateTimeAndType(startSearchDateTime, endSearchDateTime, dataType.getValue()));
    }

    private Map<String, List<Production>> groupProductionDataByMachinesName(List<Production> productionDataList) {
        return productionDataList.stream().collect(groupingBy(Production::getMachineName));
    }
}
