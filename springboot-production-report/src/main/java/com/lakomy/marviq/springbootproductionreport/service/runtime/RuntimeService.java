package com.lakomy.marviq.springbootproductionreport.service.runtime;

import com.lakomy.marviq.springbootproductionreport.dto.MachineProductionDto;

import java.time.LocalDateTime;
import java.util.List;

public interface RuntimeService {

    List<MachineProductionDto> getDowntimeMachinesProductionData(LocalDateTime date);
}
