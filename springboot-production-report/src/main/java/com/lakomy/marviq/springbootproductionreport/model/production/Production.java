package com.lakomy.marviq.springbootproductionreport.model.production;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PRODUCTION")
public class Production {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "MACHINE_NAME")
    private String machineName;

    @Column(name = "VARIABLE_NAME")
    private String variableName;

    @Column(name = "DATETIME_FROM")
    private LocalDateTime dateTimeFrom;

    @Column(name = "DATETIME_TO")
    private LocalDateTime dateTimeTo;

    @Column(name = "VALUE")
    private BigDecimal value;
}
