package com.lakomy.marviq.springbootproductionreport.controller;

import com.lakomy.marviq.springbootproductionreport.dto.MachineProductionDto;
import com.lakomy.marviq.springbootproductionreport.dto.MachineTemperatureStatusDto;
import com.lakomy.marviq.springbootproductionreport.service.production.ProductionService;
import com.lakomy.marviq.springbootproductionreport.service.runtime.RuntimeService;
import com.lakomy.marviq.springbootproductionreport.service.temperature.ProductionTemperatureService;
import com.lakomy.marviq.springbootproductionreport.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ReportMachinesDataController {

    /**
     * Rest Controller for Machines Data
     * Validation for date param should be added in a future
     */

    @Autowired
    private ProductionService productionService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProductionTemperatureService productionTemperatureService;

    @GetMapping(value = "netto/{date}")
    public List<MachineProductionDto> getMachinesNettoProductionData(@PathVariable(name = "date") String date) {
        return productionService.getNettoMachinesProductionData(DateUtils.parseStringDateWithHyphensToLocalDate(date).atStartOfDay());
    }

    @GetMapping(value = "scrap/{date}")
    public List<MachineProductionDto> getMachinesScrapProductionData(@PathVariable(name = "date") String date) {
        return productionService.getScrapPercentageProductionData(DateUtils.parseStringDateWithHyphensToLocalDate(date).atStartOfDay());
    }

    @GetMapping(value = "downtime/{date}")
    public List<MachineProductionDto> getMachinesDowntimeProductionData(@PathVariable(name = "date") String date) {
        return runtimeService.getDowntimeMachinesProductionData(DateUtils.parseStringDateWithHyphensToLocalDate(date).atStartOfDay());
    }

    @GetMapping(value = "netto_hourly/{date}")
    public List<MachineProductionDto> getMachinesNettoPerHours(@PathVariable(name = "date") String date) {
        return productionService.getNettoMachinesProductionDataPerHour(DateUtils.parseStringDateWithHyphensToLocalDate(date).atStartOfDay());
    }

    @GetMapping(value = "temperature/{date}")
    public List<MachineTemperatureStatusDto> getMachinesTemperatureStatus(@PathVariable(name = "date") String date) {
        return productionTemperatureService.getMachinesTemperatureStatusProductionData(DateUtils.parseStringDateWithHyphensToLocalDate(date).atStartOfDay());
    }

}
