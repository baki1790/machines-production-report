package com.lakomy.marviq.springbootproductionreport.service.temperature;

import com.lakomy.marviq.springbootproductionreport.dto.MachineTemperatureStatusDto;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductionTemperatureService {

    List<MachineTemperatureStatusDto> getMachinesTemperatureStatusProductionData(LocalDateTime date);
}
