package com.lakomy.marviq.springbootproductionreport.aggregator;

import com.lakomy.marviq.springbootproductionreport.model.MachineDataType;
import com.lakomy.marviq.springbootproductionreport.model.production.Production;
import com.lakomy.marviq.springbootproductionreport.model.runtime.Runtime;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface DataAggregator {

    Map<String, List<Production>> getGroupedByMachinesProductionDataOnGivenDate(LocalDateTime startOfDayDatetime);
    Map<String, List<Runtime>> getGroupedByMachinesRuntimeDataOnGivenDate(LocalDateTime startOfDayDatetime);
    Map<String, List<Production>> getGroupedByMachinesTemperatureProductionDataOnGivenDateAndType(LocalDateTime startSearchDateTime, MachineDataType dataType);
}
