package com.lakomy.marviq.springbootproductionreport.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class MachineNetProductionDto extends MachineProductionDto {

    private BigDecimal netProductionAmount;

    @Builder
    MachineNetProductionDto(String machineName, LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo, BigDecimal netProductionAmount) {
        super(machineName, dateTimeFrom, dateTimeTo);
        this.netProductionAmount = netProductionAmount;
    }
}
