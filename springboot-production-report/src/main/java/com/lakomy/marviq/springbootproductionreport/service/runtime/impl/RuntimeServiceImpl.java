package com.lakomy.marviq.springbootproductionreport.service.runtime.impl;

import com.lakomy.marviq.springbootproductionreport.aggregator.DataAggregator;
import com.lakomy.marviq.springbootproductionreport.dto.MachineDowntimePercentageProductionDto;
import com.lakomy.marviq.springbootproductionreport.dto.MachineProductionDto;
import com.lakomy.marviq.springbootproductionreport.model.runtime.Runtime;
import com.lakomy.marviq.springbootproductionreport.service.runtime.RuntimeService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RuntimeServiceImpl implements RuntimeService {

    private static final BigDecimal dayNumberOfMinutes = new BigDecimal(24L*60);

    private final DataAggregator dataAggregator;

    public RuntimeServiceImpl(DataAggregator dataAggregator) {
        this.dataAggregator = dataAggregator;
    }

    @Override
    public List<MachineProductionDto> getDowntimeMachinesProductionData(LocalDateTime startOfDayDatetime) {
        return dataAggregator.getGroupedByMachinesRuntimeDataOnGivenDate(startOfDayDatetime).entrySet().stream()
                .map(entry -> MachineDowntimePercentageProductionDto.builder()
                        .machineName(entry.getKey())
                        .dateTimeFrom(startOfDayDatetime)
                        .dateTimeTo(startOfDayDatetime.plusDays(1))
                        .downtimePercentage(getOverallDowntimePercentageForMachine(entry.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    private BigDecimal getOverallDowntimePercentageForMachine(List<Runtime> runtimeMachinesData) {
        return new BigDecimal(getNumberOfMinutesDowntime(runtimeMachinesData))
                .divide(dayNumberOfMinutes, 3, RoundingMode.CEILING)
                .multiply(new BigDecimal(100));
    }

    private Long getNumberOfMinutesDowntime(List<Runtime> runtimeMachinesData) {
        final int runtimeMachinesDataSize = runtimeMachinesData.size();
        long numberOfMinutesDown = 0;
        for(int i = 0; i < runtimeMachinesDataSize; i++){
                if(i + 1 < runtimeMachinesDataSize && runtimeMachinesData.get(i+1).getIsRunning() && !runtimeMachinesData.get(i).getIsRunning()) {
                    numberOfMinutesDown += ChronoUnit.MINUTES.between(runtimeMachinesData.get(i).getDateTime(),
                                    runtimeMachinesData.get(i+1).getDateTime());
                }
        }
        return numberOfMinutesDown;
    }
}
