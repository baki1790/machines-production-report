package com.lakomy.marviq.springbootproductionreport.model;

public enum TemperatureStatus {
    GREEN, ORANGE, RED
}
