package com.lakomy.marviq.springbootproductionreport.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

@Getter
public class MachineNetHourProductionDto extends MachineProductionDto {

    private Map<Integer, BigDecimal> hourlyNetProductionAmount;

    @Builder
    public MachineNetHourProductionDto(String machineName, LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo, Map<Integer, BigDecimal> hourlyNetProductionAmount) {
        super(machineName, dateTimeFrom, dateTimeTo);
        this.hourlyNetProductionAmount = hourlyNetProductionAmount;
    }

}
