package com.lakomy.marviq.springbootproductionreport.model.runtime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RUNTIME")
public class Runtime {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "MACHINE_NAME")
    private String machineName;

    @Column(name = "DATETIME")
    private LocalDateTime dateTime;

    @Column(name = "ISRUNNING")
    private Boolean isRunning;
}
