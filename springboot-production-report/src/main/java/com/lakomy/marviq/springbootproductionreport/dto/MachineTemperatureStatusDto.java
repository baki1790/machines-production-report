package com.lakomy.marviq.springbootproductionreport.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MachineTemperatureStatusDto {
    private String machineName;
    private String status;
}
