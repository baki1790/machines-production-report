package com.lakomy.marviq.springbootproductionreport.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class MachineScrapPercentageProductionDto extends MachineProductionDto {

    private BigDecimal scrapPercentage;

    @Builder
    MachineScrapPercentageProductionDto(String machineName, LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo, BigDecimal scrapPercentage) {
        super(machineName, dateTimeFrom, dateTimeTo);
        this.scrapPercentage = scrapPercentage;
    }
}
