package com.lakomy.marviq.springbootproductionreport.repository;

import com.lakomy.marviq.springbootproductionreport.model.runtime.Runtime;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RuntimeRepository extends CrudRepository<Runtime, Long> {

    @Query(value = "from Runtime where DATETIME >=:startDate and DATETIME <=:endDate")
    List<Runtime> getMachinesDataFromProductionByDateTime(@Param("startDate") LocalDateTime startDate, @Param("endDate")LocalDateTime endDate);
}
