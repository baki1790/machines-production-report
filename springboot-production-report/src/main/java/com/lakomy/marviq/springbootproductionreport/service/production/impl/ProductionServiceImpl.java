package com.lakomy.marviq.springbootproductionreport.service.production.impl;

import com.lakomy.marviq.springbootproductionreport.aggregator.DataAggregator;
import com.lakomy.marviq.springbootproductionreport.dto.MachineNetHourProductionDto;
import com.lakomy.marviq.springbootproductionreport.dto.MachineNetProductionDto;
import com.lakomy.marviq.springbootproductionreport.dto.MachineProductionDto;
import com.lakomy.marviq.springbootproductionreport.dto.MachineScrapPercentageProductionDto;
import com.lakomy.marviq.springbootproductionreport.model.MachineDataType;
import com.lakomy.marviq.springbootproductionreport.model.production.Production;
import com.lakomy.marviq.springbootproductionreport.service.production.ProductionService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@Service
public class ProductionServiceImpl implements ProductionService {

    private final DataAggregator dataAggregator;

    public ProductionServiceImpl(DataAggregator dataAggregator) {
        this.dataAggregator = dataAggregator;
    }

    @Override
    public List<MachineProductionDto> getNettoMachinesProductionData(LocalDateTime startOfDayDatetime) {
        return dataAggregator.getGroupedByMachinesProductionDataOnGivenDate(startOfDayDatetime).entrySet().stream()
                .map(entry -> MachineNetProductionDto.builder()
                        .machineName(entry.getKey())
                        .dateTimeFrom(startOfDayDatetime)
                        .dateTimeTo(startOfDayDatetime.plusDays(1))
                        .netProductionAmount(getNettoProduction(entry.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public List<MachineProductionDto> getScrapPercentageProductionData(LocalDateTime startOfDayDatetime) {
        return dataAggregator.getGroupedByMachinesProductionDataOnGivenDate(startOfDayDatetime).entrySet().stream()
                .map(entry -> MachineScrapPercentageProductionDto.builder()
                        .machineName(entry.getKey())
                        .dateTimeFrom(startOfDayDatetime)
                        .dateTimeTo(startOfDayDatetime.plusDays(1))
                        .scrapPercentage(getScrapPercentage(entry.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public List<MachineProductionDto> getNettoMachinesProductionDataPerHour(LocalDateTime startOfDayDatetime) {
        return dataAggregator.getGroupedByMachinesProductionDataOnGivenDate(startOfDayDatetime).entrySet().stream()
                .map(entry -> MachineNetHourProductionDto.builder()
                        .machineName(entry.getKey())
                        .dateTimeFrom(startOfDayDatetime)
                        .dateTimeTo(startOfDayDatetime.plusDays(1))
                        .hourlyNetProductionAmount(getNettoProductionPerHour(entry.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    private Map<Integer, BigDecimal> getNettoProductionPerHour(List<Production> productionList) {
        return groupByProductionPerHour(productionList).entrySet().stream()
                .collect(toMap(entry -> entry.getKey(),
                        entry -> getNettoProduction(entry.getValue())));
    }

    private Map<Integer, List<Production>> groupByProductionPerHour(List<Production> machineProductionList) {
        return machineProductionList.stream()
                .collect(groupingBy(data -> data.getDateTimeFrom().getHour()));
    }


    private BigDecimal getProductionValueByMachineDataType(MachineDataType machineDataType, List<Production> machineProductionList) {
        return machineProductionList.stream().filter(prod -> prod.getVariableName().equalsIgnoreCase(machineDataType.getValue()))
                .map(Production::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal getNettoProduction(List<Production> machineProductionData) {
        return getProductionValueByMachineDataType(MachineDataType.PRODUCTION, machineProductionData)
                .subtract(getProductionValueByMachineDataType(MachineDataType.SCRAP, machineProductionData));
    }

    private BigDecimal getScrapPercentage(List<Production> machineProductionData) {
        return getProductionValueByMachineDataType(MachineDataType.SCRAP, machineProductionData)
            .divide(getProductionValueByMachineDataType(MachineDataType.PRODUCTION, machineProductionData), 3, RoundingMode.CEILING)
            .multiply(new BigDecimal(100));

    }

}
