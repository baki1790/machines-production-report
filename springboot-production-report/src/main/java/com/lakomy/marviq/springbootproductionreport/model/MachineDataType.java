package com.lakomy.marviq.springbootproductionreport.model;

public enum MachineDataType {
    PRODUCTION("PRODUCTION"), SCRAP("SCRAP"), CORE_TEMPERATURE("CORE TEMPERATURE");

    private String machineDataType;

    MachineDataType(String machineDataType) {
        this.machineDataType = machineDataType;
    }

    public String getValue() {
        return machineDataType;
    }

    @Override
    public String toString() {
        return this.machineDataType;
    }
}
