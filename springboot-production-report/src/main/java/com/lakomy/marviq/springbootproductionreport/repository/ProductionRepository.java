package com.lakomy.marviq.springbootproductionreport.repository;

import com.lakomy.marviq.springbootproductionreport.model.production.Production;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProductionRepository extends CrudRepository<Production, Long> {

    @Query(value = "from Production where DATETIME_FROM >=:startDate and DATETIME_TO <=:endDate")
    List<Production> getMachinesDataFromProductionByDateTime(@Param("startDate") LocalDateTime startDate, @Param("endDate")LocalDateTime endDate);

    @Query(value = "from Production where VARIABLE_NAME =:machineDataType and DATETIME_FROM >=:startDate and DATETIME_TO <=:endDate")
    List<Production> getMachinesDataFromProductionByDateTimeAndType(@Param("startDate") LocalDateTime startDate, @Param("endDate")LocalDateTime endDate, @Param("machineDataType") String dataType);

}
