package com.lakomy.marviq.springbootproductionreport.service.temperature.impl;

import com.lakomy.marviq.springbootproductionreport.aggregator.DataAggregator;
import com.lakomy.marviq.springbootproductionreport.dto.MachineTemperatureStatusDto;
import com.lakomy.marviq.springbootproductionreport.model.MachineDataType;
import com.lakomy.marviq.springbootproductionreport.model.TemperatureStatus;
import com.lakomy.marviq.springbootproductionreport.model.production.Production;
import com.lakomy.marviq.springbootproductionreport.service.temperature.ProductionTemperatureService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductionTemperatureServiceImpl implements ProductionTemperatureService {

    private final DataAggregator dataAggregator;

    public ProductionTemperatureServiceImpl(DataAggregator dataAggregator) {
        this.dataAggregator = dataAggregator;
    }

    @Override
    public List<MachineTemperatureStatusDto> getMachinesTemperatureStatusProductionData(LocalDateTime startOfDayDatetime) {
        return dataAggregator.getGroupedByMachinesTemperatureProductionDataOnGivenDateAndType(startOfDayDatetime, MachineDataType.CORE_TEMPERATURE)
                .entrySet().stream()
                .map(entry -> MachineTemperatureStatusDto.builder()
                        .machineName(entry.getKey())
                        .status(getTemperatureStatus(entry.getValue()))
                        .build())
                .collect(Collectors.toList());
    }

    private String getTemperatureStatus(List<Production> list) {
        if(verifyTemperatureIsOver100(list)) {
            return TemperatureStatus.RED.name();
        } else {
            for(int i = 0; i + 3 < list.size(); i++) {
                if(verifyTemperatureIsBetween85And100(list.get(i).getValue()) &&
                        verifyTemperatureIsBetween85And100(list.get(i + 1).getValue()) &&
                        verifyTemperatureIsBetween85And100(list.get(i + 2).getValue()) &&
                        verifyTemperatureIsBetween85And100(list.get(i + 3).getValue())) {
                    return TemperatureStatus.ORANGE.name();
                }
            }
        }
        return TemperatureStatus.GREEN.name();
    }

    private boolean verifyTemperatureIsBetween85And100(BigDecimal temperatureValue) {
        BigDecimal hundred = new BigDecimal(100);
        BigDecimal eightyFive = new BigDecimal(85);
        return (temperatureValue.compareTo(eightyFive) > 0 && temperatureValue.compareTo(hundred) <= 0);
    }

    private boolean verifyTemperatureIsOver100(List<Production> productionDataList) {
        return productionDataList.stream().anyMatch(production -> production.getValue().compareTo(new BigDecimal(100)) > 0);
    }

}
