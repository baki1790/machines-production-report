package com.lakomy.marviq.springbootproductionreport.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    private static final String DATE_TIME_FORMAT_WITH_HYPHENS = "dd-MM-yyyy";
    private static final DateTimeFormatter FORMATTER_WITH_HYPHENS = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_WITH_HYPHENS);


    public static LocalDate parseStringDateWithHyphensToLocalDate(String dateStr) {
        return LocalDate.parse(dateStr, FORMATTER_WITH_HYPHENS);
    }

}
