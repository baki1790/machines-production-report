package com.lakomy.marviq.springbootproductionreport.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
public class MachineDowntimePercentageProductionDto extends MachineProductionDto {

    private BigDecimal downtimePercentage;

    @Builder
    MachineDowntimePercentageProductionDto(String machineName, LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo, BigDecimal downtimePercentage) {
        super(machineName, dateTimeFrom, dateTimeTo);
        this.downtimePercentage = downtimePercentage;
    }
}
