package com.lakomy.marviq.springbootproductionreport.service.production;

import com.lakomy.marviq.springbootproductionreport.dto.MachineProductionDto;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductionService {

    List<MachineProductionDto> getNettoMachinesProductionData(LocalDateTime date);
    List<MachineProductionDto> getScrapPercentageProductionData(LocalDateTime date);
    List<MachineProductionDto> getNettoMachinesProductionDataPerHour(LocalDateTime date);
}
